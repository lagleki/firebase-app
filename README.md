Sample app featuring Firebase and Firestore interaction

## Deployment to production

The best way to deploy it is on Google Cloud Platform.

* install gloud cli tool
* create a new project in Google Cloud console.
* write out the project's id (labelled as `<PROJECTID>` below).
* create a new Firebase project, enable phone number (add a fake number to prevent consuming paid SMS limits) and email/password (currently used only for manual tests) signin methods
* `cd` to the current directory and:
```
gcloud auth configure-docker
docker build -t app .
docker tag app gcr.io/<PROJECTID>/app
docker push gcr.io/<PROJECTID>/app
```
* in Firebase console `Project settings > General` copy the value of `firebaseConfig` variable.
* in Firebase console `Project settings > Service accounts` generate a new private key, which is a json file. Write out the json content
* deploy a new container from the uploaded docker image
  * fill in environment variables according to `.env.example`:
    * SERVICE is the content of your private key written out above with newlines removed
    * PORT=80
    * ORIGINS is the comma-separated list of origins from where you will call API (http://localhost:3000 and the host of your deployed frontend come to mind)
    * other keys are from `firebaseConfig` variable.
  * deploy the container
* in Firebase console `Authentication > Settings` add domains where your frontend has been deployed
