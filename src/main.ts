import { ValidationPipe } from '@nestjs/common'
import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'

async function bootstrap() {
  const port = process.env.PORT || 80
  const app = await NestFactory.create(AppModule)
  app.enableCors({
    allowedHeaders: ['content-type', 'authorization'],
    origin: (process.env.ORIGINS ?? '').split(','),
    credentials: true,
  })
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
    }),
  )
  console.log(`Running at port ${port}`)
  await app.listen(port)
}
bootstrap()
