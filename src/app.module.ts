import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import { AuthModule } from './auth/auth.module'
import { HealthModule } from './health/health.module'
// import { NotesModule } from './notes/notes.module'
import { ProfileModule } from './profile/profile.module'

@Module({
  imports: [
    HealthModule,
    AuthModule,
    //  NotesModule,
    ProfileModule,
    ConfigModule.forRoot(),
  ],
})
export class AppModule {}
