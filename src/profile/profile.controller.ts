import { Body, Controller, Get, Put, Req, UseGuards } from '@nestjs/common'
import { Request } from 'express'
import { AuthGuard } from '../auth/auth.guard'
import { UpdateProfileDto } from './dto'
import { ProfileService } from './profile.service'

@UseGuards(AuthGuard)
@Controller('profile')
export class ProfileController {
  constructor(private profileService: ProfileService) {}

  @Put()
  async update(
    @Req() request: Request,
    @Body() updateProfileDto: UpdateProfileDto,
  ) {
    const humanId: string = request.headers.humanId?.toString() ?? ''
    return await this.profileService.update(humanId, updateProfileDto)
  }

  @Get()
  async get(@Req() request: Request) {
    const humanId: string = request.headers.humanId?.toString() ?? ''
    return await this.profileService.get(humanId)
  }
}
