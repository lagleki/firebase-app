import { IsString, IsNotEmpty, IsEmail } from 'class-validator'

export class UpdateProfileDto {
  @IsString()
  @IsNotEmpty()
  name: string

  @IsEmail()
  @IsNotEmpty()
  email: string
}
