import { BadRequestException, Injectable } from '@nestjs/common'
import { FirebaseService } from '../firebase/firebase.service'
import { FirestoreService } from '../firestore/firestore.service'
import { ProfileDto, UpdateProfileDto } from './dto'

@Injectable()
export class ProfileService {
  constructor(
    private firestoreService: FirestoreService,
    private firebaseService: FirebaseService,
  ) {}

  async update(userId: string, updateProfileDto: UpdateProfileDto) {
    const result = await this.firestoreService.update(
      userId,
      'profile',
      undefined,
      updateProfileDto,
    )

    if (result) {
      return { message: 'Profile updated successfully.' }
    } else {
      throw new BadRequestException()
    }
  }

  async get(userId: string) {
    const result = await this.firestoreService.get(userId, 'profile')

    if (result) {
      const data = result.map((doc) => {
        return new ProfileDto(doc.id, doc.data()['name'], doc.data()['email'])
      })
      return { data: data }
    } else {
      throw new BadRequestException()
    }
  }
}
