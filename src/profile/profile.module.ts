import { Module } from '@nestjs/common'
import { ProfileService } from './profile.service'
import { ProfileController } from './profile.controller'
import { FirestoreModule } from '../firestore/firestore.module'
import { FirebaseAdminModule } from '../firebase-admin/firebase-admin.module'
import { FirebaseModule } from '../firebase/firebase.module'

@Module({
  imports: [FirestoreModule, FirebaseModule, FirebaseAdminModule],
  providers: [ProfileService],
  controllers: [ProfileController],
})
export class ProfileModule {}
