import { Injectable } from '@nestjs/common'
import { auth, credential, firestore } from 'firebase-admin'
import { initializeApp } from 'firebase-admin/app'

@Injectable()
export class FirebaseAdminService {
  constructor() {
    const firebaseServiceAccount: object = JSON.parse(
      process.env.SERVICE ?? '{}',
    )
    initializeApp({
      credential: credential.cert(firebaseServiceAccount),
    })
  }

  getFirestore() {
    return firestore()
  }

  async verifyToken(token: string) {
    try {
      const decodedIdToken = await auth().verifyIdToken(token)
      return decodedIdToken
    } catch (error) {
      console.log(error)
    }
  }

  async getUser(userId: string) {
    try {
      const user = await auth().getUser(userId)
      return user
    } catch (error) {
      console.log(error)
    }
  }
}
