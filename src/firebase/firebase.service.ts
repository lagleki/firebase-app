import {
  BadRequestException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common'
import { FirebaseApp, FirebaseOptions, initializeApp } from 'firebase/app'
import {
  createUserWithEmailAndPassword,
  getAuth,
  signInWithEmailAndPassword,
} from 'firebase/auth'

@Injectable()
export class FirebaseService {
  private instance: FirebaseApp

  constructor() {
    // Your web app's Firebase configuration
    const firebaseConfig: FirebaseOptions = {
      apiKey: process.env.APIKEY,
      authDomain: process.env.AUTHDOMAIN,
      projectId: process.env.PROJECTID,
      storageBucket: process.env.STORAGEBUCKET,
      messagingSenderId: process.env.MESSAGINGSENDERID,
      appId: process.env.APPID,
    }

    // Initialize Firebase
    this.instance = initializeApp(firebaseConfig)
  }

  async signInWithEmailAndPassword(email: string, password: string) {
    try {
      const auth = getAuth(this.instance)
      return await signInWithEmailAndPassword(auth, email, password)
    } catch (error) {
      throw new UnauthorizedException()
    }
  }

  async createUserWithEmailAndPassword(email: string, password: string) {
    try {
      const auth = getAuth(this.instance)
      return await createUserWithEmailAndPassword(auth, email, password)
    } catch (error) {
      throw new BadRequestException()
    }
  }

  async getUserData() {
    try {
      const auth = getAuth(this.instance)
      return auth.currentUser
    } catch (error) {
      console.log(error)
      throw new BadRequestException()
    }
  }
}
