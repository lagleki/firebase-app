FROM node:alpine as builder
WORKDIR /app
COPY package.json .
COPY yarn.lock .
RUN yarn
COPY . .
RUN yarn build

## Setup production application
FROM node:alpine
ENV NODE_ENV=production
WORKDIR /app
COPY --from=builder /app/package.json .
COPY --from=builder /app/yarn.lock .
COPY --from=builder /app/dist ./dist
RUN yarn --only=production --production

CMD ["yarn", "start:prod"]